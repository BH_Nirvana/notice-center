package com.xbh.notice.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbh.common.web.core.Result;
import com.xbh.notice.model.NoticeMsg;
import com.xbh.notice.service.NoticeMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2022-01-12 11:28
 */

@RestController
@RequestMapping("/noticeMsg")
public class NoticeMsgController {

    @Autowired
    private NoticeMsgService noticeMsgService;

    @PostMapping("/page")
    public Result page(@RequestBody JSONObject params){
        Page<NoticeMsg> page = noticeMsgService.page(params);
        return Result.success(page);
    }

    @PostMapping("/getNoticeById")
    public Result getNoticeById(@RequestBody JSONObject params){
        Long noticeId = params.getLong("noticeId");
        if( ObjectUtil.isNotEmpty(noticeId)) {
            NoticeMsg noticeMsg = noticeMsgService.getNoticeById(noticeId);
            return Result.success(noticeMsg);

        }
        return Result.failedParams("传入的参数有误");
    }

}
