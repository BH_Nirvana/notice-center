package com.xbh.notice.controller;

import com.alibaba.fastjson.JSONObject;
import com.xbh.common.web.core.Result;
import com.xbh.notice.constants.MessageDirectKey;
import com.xbh.notice.mq.SendMessage;
import com.xbh.notice.service.MessageQueueService;
import com.xbh.notice.service.SendMessageService;
import com.xbh.notice.service.SendMessageServiceImpl;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2022-01-07 17:03
 */

@RestController
@RequestMapping(path = "/notice")
public class SendMessageController {




    @Autowired
    private SendMessageService sendMessageService;


    @PostMapping("/sendMessage")
    public Result sendMessage(@RequestBody JSONObject parmas){

        sendMessageService.sendMessage(parmas);
        return Result.success();
    }
}
