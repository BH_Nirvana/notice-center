package com.xbh.notice.service;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xbh.notice.constants.NoticeTypes;
import com.xbh.notice.mapper.NoticeMsgMapper;
import com.xbh.notice.model.NoticeMsg;
import org.aspectj.weaver.ast.Var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:46
 *
 */
@Service
public class NoticeMsgServiceImpl extends ServiceImpl<NoticeMsgMapper, NoticeMsg> implements NoticeMsgService {


    /**
     *
     * @param parmas
     * @return
     */
    @Override
    public Page<NoticeMsg> page(JSONObject parmas) {
        NoticeMsg noticeMsg = parmas.toJavaObject(NoticeMsg.class);
        Integer pageNum = parmas.getInteger("pageNum");
        Integer pageSize = parmas.getInteger("pageSize");
        LambdaQueryWrapper<NoticeMsg> wrapper = new LambdaQueryWrapper();

        if(StrUtil.isEmpty(parmas.getString("isNoticeType"))){
            wrapper.eq(NoticeMsg::getStates, NoticeTypes.NOTICE_UNPROCESSED.getCode());
        }
        wrapper.eq(NoticeMsg::getUserId,noticeMsg.getUserId());
        wrapper.eq(NoticeMsg::getDeleted,0);
        wrapper.orderByDesc(NoticeMsg::getCreateTime);
        Page<NoticeMsg> noticeMsgPage = this.baseMapper.selectPage(new Page<NoticeMsg>(pageNum, pageSize), wrapper);
        return noticeMsgPage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public NoticeMsg getNoticeById(Long noticeId) {
        //查看详细详情的时候及浏览了信息
        NoticeMsg noticeMsg = new NoticeMsg();
        noticeMsg.setNoticeId(noticeId);
        noticeMsg.setStates(NoticeTypes.NOTICE_SUCCESS.getCode());
        this.baseMapper.updateById(noticeMsg);
        return this.baseMapper.selectById(noticeId);

    }
}
