package com.xbh.notice.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.nimbusds.jose.JOSEException;
import com.xbh.common.exception.JwtSignatureVerifyException;
import com.xbh.common.utils.JwtUtil;
import com.xbh.common.utils.JwtUtils;
import com.xbh.notice.constants.MessageDirectKey;
import com.xbh.notice.constants.NoticeTypes;
import com.xbh.notice.model.NoticeMsg;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 09:51
 */
@Service
public class SendMessageServiceImpl implements SendMessageService {


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MessageQueueService messageQueueService;

    @Autowired
    private NoticeMsgServiceImpl noticeMsgService;


    /**
     * 发送消息
     * @param parmas
     */
    public void sendMessage(JSONObject parmas){

        if(StringUtils.isNotBlank(parmas.getString("userIds"))){
            String userIds = parmas.getString("userIds");
            String[] userIdArray = userIds.split(",");
            for (String userId : userIdArray) {
                NoticeMsg noticeMsg = parmas.toJavaObject(NoticeMsg.class);
                noticeMsg.setUserId(Long.parseLong(userId));
                if(StrUtil.isNotEmpty(parmas.getString("username"))){
                    noticeMsg.setSendFromName(parmas.getString("username"));
                    noticeMsg.setCreatePerson(parmas.getString("username"));
                    noticeMsg.setUpdatePerson(parmas.getString("username"));
                }else {
                    noticeMsg.setSendFromName(JwtUtil.getUsername());
                    noticeMsg.setCreatePerson(JwtUtil.getUsername());
                    noticeMsg.setUpdatePerson(JwtUtil.getUsername());
                }
                noticeMsg.setStates(NoticeTypes.NOTICE_UNPROCESSED.getCode());
                messageQueueService.sendCommonDirect(MessageDirectKey.COMMON_SEND_MESSAGE,noticeMsg);
            }
        }
    }
}
