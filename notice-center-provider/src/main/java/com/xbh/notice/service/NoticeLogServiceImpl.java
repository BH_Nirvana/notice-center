package com.xbh.notice.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xbh.notice.mapper.NoticeLogMapper;
import com.xbh.notice.model.NoticeLog;
import org.springframework.stereotype.Service;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:47
 */

@Service
public class NoticeLogServiceImpl extends ServiceImpl<NoticeLogMapper, NoticeLog> implements NoticeLogService {
}
