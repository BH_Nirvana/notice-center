package com.xbh.notice.service;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 */

@Component
@Slf4j
public class MessageQueueService {

    private final Logger logger = LogManager.getLogger(MessageQueueService.class);

    @Autowired
    @Qualifier("rabbitTemplate")
    private RabbitTemplate rabbitTemplate;



    @Value("${rabbit.exchange}")
    private String TOPIC_EXCHANGE;

    /**
     * 发送主题消息
     * 主题交换机：Topic
     * 发送到主题交换机上的消息需要携带指定规则的routing_key，
     * 主题交换机会根据这个规则将数据发送到对应的(多个)队列上。
     * 主题交换机的routing_key需要有一定的规则，交换机和队列的binding_key需要采用*.#.*.....的格式，每个部分用.分开，其中：
     *
     * @param routingKey
     * @param msg
     * @throws AmqpException
     */
    public void sendCommonTopic(String routingKey,Object msg) {
        try{
            log.info(msg.toString());
            rabbitTemplate.convertAndSend(TOPIC_EXCHANGE,routingKey, msg);
        }catch (AmqpException e){
            logger.error("消息发送异常，Exchange:{},RoutingKey:{},消息:{}",TOPIC_EXCHANGE,routingKey, JSONObject.toJSONString(msg),e);
        }
    }
    /**
     * 发送主题消息
     * 直连交换机：Direct  1对1-----一个消息只能被一个消费者消费
     * 直连交换机是一种带路由功能的交换机，一个队列会和一个交换机绑定，除此之外再绑定一个routing_key，
     * 当消息被发送的时候，需要指定一个binding_key，这个消息被送达交换机的时候，
     * 就会被这个交换机送到指定的队列里面去。同样的一个binding_key也是支持应用到多个队列中的。
     * 这样当一个交换机绑定多个队列，就会被送到对应的队列去处理。
     * @param routingKey
     * @param msg
     * @throws AmqpException
     */
    public void sendCommonDirect(String routingKey,Object msg) {
        try{
            log.info(msg.toString());
            rabbitTemplate.convertAndSend(TOPIC_EXCHANGE,routingKey, msg);
        }catch (AmqpException e){
            logger.error("消息发送异常，Exchange:{},RoutingKey:{},消息:{}",TOPIC_EXCHANGE,routingKey, JSONObject.toJSONString(msg),e);
        }
    }
}
