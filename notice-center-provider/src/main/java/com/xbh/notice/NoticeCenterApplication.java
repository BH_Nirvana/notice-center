package com.xbh.notice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ImportResource;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-15 17:25
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.xbh.*.*")
@ImportResource(locations = "classpath:app.xml")
@Slf4j
public class NoticeCenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(NoticeCenterApplication.class, args);
        log.info("消息中心已启动");
    }
}
