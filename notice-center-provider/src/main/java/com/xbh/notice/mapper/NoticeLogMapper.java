package com.xbh.notice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbh.notice.model.NoticeLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:34
 */

@Mapper
public interface NoticeLogMapper extends BaseMapper<NoticeLog> {
}
