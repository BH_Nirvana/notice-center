package com.xbh.notice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbh.notice.model.NoticeMsg;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:32
 */

@Mapper
public interface NoticeMsgMapper extends BaseMapper<NoticeMsg> {
}
