package com.xbh.notice.config;


import com.xbh.notice.constants.MessageDirectKey;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2022-01-10 17:36
 */

@Configuration
public class RabbitConfig {


    @Value("${rabbit.host}")
    private String host;

    @Value("${rabbit.port}")
    private int port;

    @Value("${rabbit.username}")
    private String username;

    @Value("${rabbit.password}")
    private String password;

    @Value("${rabbit.vhost}")
    private String vhost;


    /**
     * 定义交换机
     */
    @Value("${rabbit.exchange}")
    public String EXCHANGE;


    /**
     * 定义队列
     */
    public static final String QUEUE = "mc.send.message";




    /**
     * 连接RabbitMQ
     * @return
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(vhost);
        connectionFactory.setPublisherConfirmType(CachingConnectionFactory.ConfirmType.CORRELATED);//开启ack发送确认
        connectionFactory.setPublisherReturns(true);//开启发送失败退回
        return connectionFactory;

    }

    /**
     * 声明mq模板
     * @return
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());//消息转换json
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setConfirmCallback(new RabbitConfirmCallBack());//发送确认
        rabbitTemplate.setReturnCallback(new RabbitReturnCallback());//发送失败确认
        return rabbitTemplate;
    }

    /**
     * 针对消费者配置
     * 1. 设置交换机类型
     * 2. 将队列绑定到交换机
     FanoutExchange: 将消息分发到所有的绑定队列，无routingkey的概念
     HeadersExchange ：通过添加属性key-value匹配
     DirectExchange:按照routingkey分发到指定队列
     TopicExchange:多关键字匹配
     */
    @Bean
    public DirectExchange defaultExchange() {
        return new DirectExchange(EXCHANGE);
    }

    /**
     * 获取队列
     * @return
     */
    @Bean
    public Queue queue() {
        return new Queue(QUEUE, true); //队列持久
    }


    /**
     * 将队列和交换机进行绑定
     * @return
     */
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(defaultExchange()).with(MessageDirectKey.COMMON_SEND_MESSAGE);
    }
}
