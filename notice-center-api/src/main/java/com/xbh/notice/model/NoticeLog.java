package com.xbh.notice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xbh.common.base.BaseEntity;
import lombok.Data;

import javax.persistence.Column;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:20
 */


@Data
public class NoticeLog extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    @Column(name = "notice_id")
    private Long noticeLogId;

    @Column(name = "notice_id")
    private Long noticeId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "notice_title")
    private String noticeTitle;

    @Column(name = "notice_info")
    private String noticeInfo;

}
