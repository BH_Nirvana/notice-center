package com.xbh.notice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xbh.common.base.BaseEntity;
import lombok.Data;

import javax.persistence.Column;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:11
 */

@Data
public class NoticeMsg extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    @Column(name = "notice_id")
    private Long noticeId;

    @Column(name = "states")
    private Integer states;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "subtitle")
    private String subtitle;

    @Column(name = "notice_info")
    private String noticeInfo;

    @Column(name = "send_from_name")
    private String sendFromName;

}
