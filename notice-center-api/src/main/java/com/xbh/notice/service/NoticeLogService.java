package com.xbh.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xbh.notice.model.NoticeLog;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:38
 */

public interface NoticeLogService extends IService<NoticeLog> {
}
