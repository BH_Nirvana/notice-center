package com.xbh.notice.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nimbusds.jose.JOSEException;
import com.xbh.common.exception.JwtSignatureVerifyException;

import java.text.ParseException;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:44
 */

public interface SendMessageService  {

    public void sendMessage(JSONObject parmas);
}
