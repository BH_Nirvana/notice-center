package com.xbh.notice.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xbh.notice.model.NoticeMsg;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:39
 */


public interface NoticeMsgService extends IService<NoticeMsg> {

    Page<NoticeMsg> page(JSONObject parmas);

    NoticeMsg getNoticeById(Long noticeId);
}
