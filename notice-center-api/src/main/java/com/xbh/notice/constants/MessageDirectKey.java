package com.xbh.notice.constants;

/**
 * @program: notice-center
 * @description: RoutingKey(将交换机与队列绑定)
 * @author: 许宝华
 * @create: 2022-01-10 11:27
 */

public class MessageDirectKey {

    public static String COMMON_SEND_MESSAGE = "commons.send.message";
}
