package com.xbh.notice.constants;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2021-12-16 14:40
 */

public enum NoticeTypes {

    NOTICE_UNPROCESSED(1,"待处理"),
    NOTICE_SUCCESS(2,"已处理"),
    NOTICE_ERROR(3,"失败");

    private int code;
    private String name;

    NoticeTypes(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }


}
