package com.xbh.notice.api;

import com.alibaba.fastjson.JSONObject;
import com.xbh.common.web.core.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @program: notice-center
 * @description:
 * @author: 许宝华
 * @create: 2022-03-07 15:16
 */
@FeignClient(value = "notice-center")
public interface SendMessageFeignClient {

    /**
     * 发送消息---站内信
     * @param parmas
     * @return
     */
    @PostMapping("/notice/sendMessage")
    public Result sendMessage(@RequestBody JSONObject parmas);
}
